//2 

db.fruits.aggregate([
    { $unwind : "$origin" },
    { $group : { _id : "$origin" , fruits : { $sum : 1 } } }
]);

//3 

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$onSale", totalStocks: {$sum: "$stock"} } },
    { $project: {_id: 0} }
])

//4

db.fruits.aggregate([
    { $match: { stock: { $gte: 20 } } },
    { $count: "enoughStock"}
]);

//5 
db.fruits.aggregate([
    { $match: { onSale: true } }
    { $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } }
]);


//6

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", max_price: { $max: "$price" } } }
]);


//7

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", min_price: { $min: "$price" } } }
]);

